package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = true)
                                       String name, Model model) {

        model.addAttribute("break","=============================================="
                + "=====================");

        if (name == null || name.equals("")) {
            model.addAttribute("title", "This is my CV");
        } else {
            model.addAttribute("title", name + ", I hope you interested to hire me");
        }

        model.addAttribute("header1", "BASIC INFORMATION");
        model.addAttribute("name", "Name: Hema Mitta Kalyani");
        model.addAttribute("birthdate", "Birthdate: March 7, 1998");
        model.addAttribute("birthplace", "Birthplace: Jakarta");
        model.addAttribute("address", "Address: Raya Hankam Street number 198, Bekasi.");

        model.addAttribute("header2", "EDUCATION HISTORY");
        model.addAttribute("highschool", "High School: SMAN 14 Jakarta");
        model.addAttribute("current", "Currently a Computer Science Undergraduate Student at "
                + "Universitas Indonesia");

        model.addAttribute("header3", "DESCRIPTION");
        model.addAttribute("desc", "Currently a CS UI Undergraduate Student with other "
                + "organizational and committee activities.");

        return "greeting";
    }

}
