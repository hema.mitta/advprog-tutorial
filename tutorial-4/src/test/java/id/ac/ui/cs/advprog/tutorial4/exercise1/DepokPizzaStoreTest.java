package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.jupiter.api.Assertions.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

public class DepokPizzaStoreTest {

    private PizzaStore pizzaStore;

    @Before
    public void setUp() {
        pizzaStore = new DepokPizzaStore();
    }

    @Test
    public void testCreatePizza() {
        Pizza pizza;
        pizza = pizzaStore.createPizza("cheese");
        assertEquals(pizza.getName(), "Depok Style Cheese Pizza");
        pizza = pizzaStore.createPizza("veggie");
        assertEquals(pizza.getName(), "Depok Style Veggie Pizza");
        pizza = pizzaStore.createPizza("clam");
        assertEquals(pizza.getName(), "Depok Style Clam Pizza");
    }
}
