package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.jupiter.api.Assertions.assertTrue;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;
import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaStoreTest {

    private PizzaStore pizzaStore;
    private Pizza pizza;

    @Before
    public void setUp() {
        pizzaStore = new NewYorkPizzaStore();
    }

    @Test
    public void testCreatePizza() {
        pizza = pizzaStore.createPizza("cheese");
        assertTrue(pizza instanceof CheesePizza);
        pizza = pizzaStore.createPizza("veggie");
        assertTrue(pizza instanceof VeggiePizza);
        pizza = pizzaStore.createPizza("clam");
        assertTrue(pizza instanceof ClamPizza);
    }
}
