package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class PizzaStoreTest {

    protected PizzaStore pizzaStore;

    @Before
    public void setUp() {
        pizzaStore = new NewYorkPizzaStore();
    }

    @Test
    public void testOrderPizza() {
        Pizza pizza;
        pizza = pizzaStore.orderPizza("cheese");
        assertTrue(pizza instanceof CheesePizza);
        pizza = pizzaStore.orderPizza("veggie");
        assertTrue(pizza instanceof VeggiePizza);
        pizza = pizzaStore.orderPizza("clam");
        assertTrue(pizza instanceof ClamPizza);
    }
}
